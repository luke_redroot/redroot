(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-2325270-1', 'auto');
ga('send', 'pageview');



var R = (function(w,d,undefined){

  Array.prototype.shuffle = function() {
    var i = this.length, j, temp;
    if ( i == 0 ) return this;
    while ( --i ) {
      j = Math.floor( Math.random() * ( i + 1 ) );
      temp = this[i];
      this[i] = this[j];
      this[j] = temp;
    }
    return this;
  }

  var addBackground = function(){
    var header = d.getElementById("header");
    var pattern = GeoPattern.generate('RedRoot',{
      baseColor: "#011425",
      generator: ['xes','hexagons','triangles','chevrons','diamonds'].shuffle()[0]
    });
    header.setAttribute("style","background-image: " + pattern.toDataUrl());
  }

  return {
    t: addBackground
  }

})(window,document);

R.t();
