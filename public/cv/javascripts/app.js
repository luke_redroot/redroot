/* 
* Skeleton V1.0.2
* Copyright 2011, Dave Gamache
* www.getskeleton.com
* Free to use under the MIT license.
* http://www.opensource.org/licenses/mit-license.php
* 5/20/2011
*/	
	

$(document).ready(function() {



	/*
	$(".work").hover(function()
		{
			if($(window).width() > 800)
			{
				$(this).find(".work_flair").stop().fadeTo(500,1);	
			}
		},
		function()
		{
			if($(window).width() > 800)
			{
				$(this).find(".work_flair").stop().fadeTo(500,0);
			}
		}
	);
	*/
		
	// detect mobile and show resize if not
	if( !navigator.userAgent.match(/Android/i) &&
	 !navigator.userAgent.match(/webOS/i) &&
	 !navigator.userAgent.match(/iPhone/i) &&
	 !navigator.userAgent.match(/iPod/i)
	 ){
		$("#resize").fadeIn("slow");
	 // some code
	}
	
	
});