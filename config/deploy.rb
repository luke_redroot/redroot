require 'bundler/capistrano'

set :application, "redroot"
set :repository,  "git@bitbucket.org:luke_redroot/redroot.git"

role :web, "162.243.60.123"                          # Your HTTP server, Apache/etc
role :app, "162.243.60.123"                          # This may be the same as your `Web` server
role :db,  "162.243.60.123", :primary => true # This is where Rails migrations will run

set :migrate_env,         "RACK_ENV=production"
set :repository_cache,    "#{application}_cache"
set :environment,         "production"
set :use_sudo, true
set :runner,              "deploy"
set :user,                "deploy"
set :deploy_to,           "/var/www/#{application}"
set :keep_releases,       5
#set :deploy_via,          :remote_cache
set :scm,                 :git
set :scm_user,            "deploy"
set :keep_releases,1

ssh_options[:forward_agent] = true
default_run_options[:pty] = true

after "deploy:restart","deploy:cleanup"
after 'deploy:setup', 'deploy:custom_setup'

namespace :deploy do
  task :custom_setup do
    sudo "chown -R deploy:deploy #{deploy_to}"
  end
  task :restart do
    nginx::reload
  end
end

namespace :nginx do
  task :restart, :roles => :web do
    sudo "/etc/init.d/nginx restart"
  end

  task :stop, :roles => :web do
    sudo "/etc/init.d/nginx stop"
  end

  task :start, :roles => :web do
    sudo "/etc/init.d/nginx start"
  end

  task :reload, :roles => :web do
    sudo "/etc/init.d/nginx reload"
  end
end
