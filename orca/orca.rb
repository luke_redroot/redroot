require "rest-client"
require "orca/extensions/apt"
require "orca/extensions/file_sync"

Dir[File.expand_path('./orca/packages/*.rb')].each {|file| require file }

group 'server' do
  node 'redroot-round-2', '46.101.53.76'
end

package 'server' do
  depends_on 'firewall'
  depends_on 'deploy-user'
  depends_on 'build-essential'
  depends_on 'git'
  depends_on 'ruby-1.9.3'
  depends_on 'nginx-passenger'
  depends_on 'bundler'
  depends_on 'libxml2'
  depends_on 'libxslt'
  depends_on 'nodejs'
  depends_on 'capistrano'

  apply do
    trigger 'nginx:restart'
  end

end

apt_package 'build-essential'
apt_package 'git', 'git-core'
apt_package 'libxml2', 'libxml2-dev'
apt_package 'libxslt', 'libxslt1-dev'
