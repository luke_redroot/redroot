Orca.extension do
  package 'redis' do
    depends_on 'apt'

    validate do
      trigger('apt:exists', 'redis-server') && binary_exists?('redis-server')
    end

    apply do
      trigger 'apt:ppa', 'ppa:chris-lea/redis-server'
      trigger 'apt:install', 'redis-server'
    end

    remove do
      trigger 'apt:remove', 'redis-server'
    end

    file({
      :source       => 'files/redis.conf',
      :destination  => '/etc/redis/redis.conf',
      :create_dirs  => true,
      :permissions  => 0644,
      :user         => 'root',
      :group        => 'root'
    })
  end
end