Orca.extension do
  apt_package 'logrotate'

  package 'logrotate-rails' do
    depends_on 'logrotate'

    file({
      :source      => 'files/logrotate-sinatra',
      :destination => "/etc/logrotate.d/sinatra",
      :permissions => 0644,
      :user        => 'root',
      :group       => 'root'
    })
  end
end