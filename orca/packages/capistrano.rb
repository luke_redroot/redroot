Orca.extension do
  package 'capistrano' do

    apply do
      run_local 'cap deploy:setup'
      trigger 'capistrano:deploy'
    end

    remove do
      run 'rm -rf /var/www/redroot'
    end

    validate do
      remote('/var/www/redroot/current').owner[:user] == 'deploy' rescue false
    end

    action('deploy') { run_local('cap deploy') }
  end
end