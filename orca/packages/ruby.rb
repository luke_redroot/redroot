Orca.extension do
  package 'ruby-1.9.3' do
    depends_on 'apt'

    validate do
      trigger('apt:exists', 'ruby1.9.3') && binary_exists?('ruby')
    end

    apply do
      trigger 'apt:ppa', 'ppa:brightbox/ruby-ng'
      trigger 'apt:install', 'ruby1.9.3'
    end
  end
end