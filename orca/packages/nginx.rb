Orca.extension do
  apt_package 'nginx' do
    action('start')   { sudo 'service nginx start' }
    action('stop')    { sudo 'service nginx stop' }
    action('restart') { trigger('nginx:running') ? sudo('service nginx restart') : trigger('nginx:start') }
    action('running') { run('ps aux | grep [n]ginx') =~ /nginx: master process/ }
    action('reload')  { sudo 'pkill -HUP nginx' }
  end


  package 'nginx-passenger' do
    depends_on 'gem', 'nginx', 'passenger-common', 'firewall'

    validate do
      trigger('gem:exists', 'passenger') &&
      remote("/etc/nginx/sites-enabled/default").exists? == false &&
      trigger('firewall:check', 'allow', 80) &&
      trigger('firewall:check', 'allow', 443)
    end

    apply do
      trigger 'firewall:add', 'allow',  80
      trigger 'firewall:add', 'allow',  443
      trigger 'gem:install', 'passenger'
      remote("/etc/nginx/sites-enabled/default").delete!
    end

    remove do
      trigger 'firewall:remove', 'allow', 80
      trigger 'firewall:remove', 'allow',  443
      trigger 'gem:uninstall', 'passenger'
    end

    file({
      :source       => 'files/nginx.conf',
      :destination  => '/etc/nginx/nginx.conf',
      :permissions  => 0644,
      :user         => 'root',
      :group        => 'root'
    }) { trigger 'nginx:restart' }
  end


  apt_package 'passenger-common', 'passenger-common1.9.1' do
    depends_on 'ruby-1.9.3'
  end

end