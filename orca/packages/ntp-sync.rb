Orca.extension do
  apt_package 'ntpdate'

  package 'ntp-sync' do
    depends_on 'ntpdate'

    file({
      :source      => 'files/ntpdate',
      :destination => "/etc/cron.hourly/ntpdate",
      :permissions => 0700,
      :user        => 'root',
      :group       => 'root'
    })
  end
end