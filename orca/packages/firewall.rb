Orca.extension do
  apt_package 'ufw'

  package 'firewall' do
    depends_on 'ufw'

    apply do
      sudo 'ufw disable'
      sudo 'ufw default deny'
      sudo 'ufw allow ssh'
      sudo 'ufw limit ssh'
      sudo 'ufw --force enable'
    end

    validate do
      sudo("ufw status") =~ /Status: active/ &&
      trigger('firewall:check', 'allow', 22)
    end

    remove do
      sudo 'ufw disable'
    end

    action('add')    {|mode, service| sudo "ufw #{mode.downcase} #{service}", :once => true }
    action('remove') {|mode, service| sudo "ufw delete #{mode.downcase} #{service}", :once => true }
    action('check')  {|mode, port| sudo("ufw status") =~ /\b#{port}\b.+?#{mode.upcase}/ }
  end
end