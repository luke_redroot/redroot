Orca.extension do
  package 'deploy-user' do

    apply do
      sudo "useradd -d /home/deploy -G sudo -m -U deploy"
      sudo "echo 'deploy ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers"
    end

    remove do
      sudo "userdel -f deploy"
      sudo "rm -rf /home/deploy"
    end

    validate do
      sudo('getent passwd deploy') =~ /^deploy:/ &&
      sudo('groups deploy') =~ /deploy sudo/
    end

    file({
      :source       => 'files/ssh.keys',
      :destination  => '/home/deploy/.ssh/authorized_keys',
      :create_dirs  => true,
      :permissions  => 0644,
      :user         => 'deploy',
      :group        => 'deploy'
    })
  end
end