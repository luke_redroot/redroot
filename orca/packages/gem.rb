Orca.extension do
  package 'gem' do
    depends_on 'ruby-1.9.3'

    action 'exists' do |gem_name, version=nil|
      version_rule = version.nil? ? '' : %[-v=#{version}]
      run("gem list -i #{gem_name} #{version_rule}") =~ /true/
    end

    action 'install' do |gem_name, version=nil|
      version_rule = version.nil? ? '' : %[-v=#{version}]
      sudo "gem install #{gem_name} #{version_rule} --no-ri --no-rdoc", :once => true
    end

    action 'uninstall' do |gem_name|
      sudo "gem uninstall #{gem_name} -x -a", :once => true
    end
  end



  package 'bundler' do
    depends_on 'gem'

    apply    { trigger 'gem:install', 'bundler' }
    remove   { trigger 'gem:uninstall', 'bundler' }
    validate { trigger 'gem:exists', 'bundler' }
  end
end