require 'date'
require 'maruku'

class Post

  attr_accessor :title, :slug, :date, :url, :content

  FOLDER = File.join(File.dirname(__FILE__), *%w[.. content *.md])

  def self.all
    Dir.glob(File.expand_path(FOLDER)).map { |f| new(f) }.sort_by { |p| p.date }.reverse
  end

  def self.posted
    all.reject { |p| p.date.to_time > Time.now }
  end

  def self.find(slug)
    all.select { |p| p.slug == slug }.first
  end

  def initialize(file)
    filename = file.split("/").last
    @date    = Date.parse(filename.split("__")[0].gsub("_",""))
    @slug    = filename.split("__")[1].gsub(".md","").gsub("_","-").gsub(/\:/,"")
    @title   = @slug.split("-").map { |s| s.upcase == s ? s : s.capitalize }.join(" ")
    @url     = "/posts/#{@slug}"
    @content = Maruku.new(File.read(file)).to_html
  end

end
