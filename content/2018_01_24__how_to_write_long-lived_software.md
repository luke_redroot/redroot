Or a better title. Like three metrics I wished I'd considered when starting code I'd maintain for years

The most important time metric isn't how long it takes to build the feature, its how long is spent maintaining it and previous technical decisions. Whilst you cant promise 100% foresight, Google SRE estimated than between 40-90% o engineering effort is spent on code already in production. No fence to throw it over

In that case there are three important characteristics of code to consider, especially when the utility and value of a piece of software or a feature aren't yet known.

a) Small interface (loose coupling)

b) disposability of the code, how easy is it to throw away

c) how easy is it to measure usage, and problems, to determine if it's being used in the way you think it is.
