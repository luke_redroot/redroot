One of our systems at work involves several distributed services sharing a
common Ruby gem, which means that whenever we make changes to that gem we
spend a fair bit of time running `bundle update config-gem` in all those
repositories. The other day I decided to write a Rake task in the shared gem to
take care of this for me. This rake task assumed that all the appropriate repos
shared the same parent folder, i.e. `/project/config-gem`, `/project/repo-b`,
`/project/repo-b`, `/project/repo-n`

Here's a rough outline of what it did (spoiler, this didn't work):

<script src="https://gist.github.com/redroot/acf3b8235ba8cb99ae2b.js">
</script>

We would run the command with `bundle exec rake updates:services`. Here's a breakdown of what is
actually happening:

+ For each repo that we want to update, first we want to check there a no
uncommitted files in the repo. if so then we'll skip it for now.
+ The next stage is to update the specified gem
+ We then commit the result
+ And finally we run a `git pull --rebase` to ensure we keep commits in order,
  and then push the commit

This all seemed pretty straight forward. The issue came with actually bundling with
the `bundle update` command. It didn't seem to be running in any of the folders that I
was `cd`-ing into, which was also checked with `pwd`. Changing the command to `bundle list`
revealed that **bundler thought I wanted to run an update inside the config-gem!**
So even though I was changing directory, Bundler would assume I was referring to
the Gemfile in the folder I was running the task from.

After some digging from my [colleague](https://twitter.com/mpatric), we found out that
Bundler can use an environment variable called `BUNDLE_GEMFILE` to specify the Gemfile
you wish to interact with. In this case, this was always set to `/projects/config-gem/Gemfile`, so the
Gemfile of the folder I was running the rake task from. This is because we were calling
the task with `bundle exec`! As it was already set, any further calls to bundler
simply uses this environment variable instead of trying to detect the Gemfile
in the folder the command was executing in.

That meant we had to update the command to:

    puts `cd #{repo_path} && BUNDLE_GEMFILE=#{repo_path}/Gemfile bundle update config-gem`

Which worked, and now saves us time, definitely enough to justify the time spent discovering
this issue! The final code was:

<script src="https://gist.github.com/redroot/60f376ece87314a5299a.js">
</script>

Hopefully this post will be helpful to anyone else who encounters these issues as well!
