In the ideal web developer's world, we want all visitors to a website to start at the home page; thats what its there for. To smoothly guide your users to the different subsections or features if they prefer browsing, show experienced users where to get the latest relevant information, and have an appropiate way for users to search the page. But that may not always be the case: the home page will not be the entry point for most users to your site (with some exceptions, such as large news sites).

Consider you search for a specific topic in Google, and you find an interesting blog post or product on a e-commerce site. You like the look of the blog post, you've found some useful information, and you want to look around the site itself. In **Don't Make Me Think**, Steve Krug has a very entertaining and useful acid test for navigation known as the mafia-like **Trunk Test**. Imagine you have been kidnapped, blindfolded and locked in the trunk (boot if you're British) of a car, and been driven off into the middle of an unfamiliar location. This rather amusing situation can be compared with landing on a page within a website, you could have a similar set of questions:

+ What site is this?
+ What page am I on?
+ What are the major sections of this site?
+ What are my options at this level?
+ Where am I in the scheme of things?
+ How can I search?<footnote>In my opinion probably one of the most important aspects of any site navigation, if not the most important</footnote>

Each of these corresponds to a specific element of navigation that you can implement to addess these questions:

+ Site ID and Logo
+ Page Name
+ Top Level Navigation/Sections
+ Local navigation
+ "You are Here" indicators & Breadcrumbs
+ Search Box

Please do note that all this does not undermine the role of a homepage in anyway. it is still a primary entry point, but now it has the additional job of clarifying the goals and content of your site for users from all entry points, aand not just those landing on your page initally. It is also not necessarily a bad thing that the homepage is not the only entry point. If you have a number of pages which are useful for a number of different popular search queries then you are more likely to have a higher search engine ranking overall, and therefore more traffic.

Most of this is of course convention now, but for emerging developers like me who didnt have the privelege of being around as these conventions arose this is a nice tool when considering a design. I picked up the knack for based on the fact that our designers did it, not because I read it somewhere, and thus never really thought about it. It's often nice to have a small slice of academia to justify a part of web design which I normally just took for granted, seeing as it is a convention, without necessarily thinking about how it came to be that way.