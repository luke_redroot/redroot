require 'rubygems'
require 'bundler'
require 'bundler/setup'
Bundler.require
require 'sass/plugin/rack'
require 'haml'
require_relative './lib/post'

Sass::Plugin.add_template_location("./public/css/sass","./public/css")
set :sass, {:style => :compressed}

use Sass::Plugin::Rack

before do
  @body_classes = []
end

get '/' do
  @body_classes << "is-home"
  @posts = Post.posted
  @title = 'Home'
  haml :index
end

get '/cv' do
	redirect '/cv/index.html'
end

get '/sandbox/holmes' do
	redirect '/sandbox/holmes/index.html'
end

get '/about' do
	@title = "About"
	haml :about
end

get '/projects' do
	@title = "Projects"
	haml :projects
end

get '/posts/:title' do
  @post = Post.find(params[:title])
  @title = @post.title
  haml :post
end